package com.jamie.taipeizoo.network

import com.jamie.taipeizoo.data.Area
import com.jamie.taipeizoo.data.Plant
import retrofit2.http.GET

interface ZooDataApi {
    @GET("5a0e5fbb-72f8-41c6-908e-2fb25eff9b8a?scope=resourceAquire&resource_id=5a0e5fbb-72f8-41c6-908e-2fb25eff9b8a")
    suspend fun getAreas(): Area

    @GET("f18de02f-b6c9-47c0-8cda-50efad621c14?scope=resourceAquire&resource_id=f18de02f-b6c9-47c0-8cda-50efad621c14")
    suspend fun getPlants(): Plant
}
