package com.jamie.taipeizoo.network

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}
