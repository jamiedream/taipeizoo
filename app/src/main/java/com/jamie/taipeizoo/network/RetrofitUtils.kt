package com.jamie.taipeizoo.network

import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

object RetrofitUtils {

    private const val DOMAIN = "https://data.taipei/api/v1/dataset/"
    private lateinit var client: OkHttpClient

    private fun buildRetrofit(): Retrofit {
        val moshi = Moshi.Builder()
            .addLast(KotlinJsonAdapterFactory())
            .build()
        createOkHttpClient()
        return Retrofit.Builder()
            .baseUrl(DOMAIN)
            .client(client)
            .addConverterFactory(
                MoshiConverterFactory.create(moshi)
            )
            .build()
    }

    private fun createOkHttpClient() {
        client = OkHttpClient.Builder().build()
    }

    fun getZooData(): ZooDataApi {
        return buildRetrofit().create(ZooDataApi::class.java)
    }
}
