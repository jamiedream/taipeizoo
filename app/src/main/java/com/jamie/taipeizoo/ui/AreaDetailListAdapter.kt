package com.jamie.taipeizoo.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.jamie.taipeizoo.data.PlantInfo
import com.jamie.taipeizoo.databinding.ItemPlantBinding
import com.jamie.taipeizoo.util.bindImageWithPlaceholder

class AreaDetailListAdapter(private val listener: PlantClickListener) :
    ListAdapter<PlantInfo, AreaDetailListAdapter.AreaDetailViewHolder>(DiffCallback()) {

    interface PlantClickListener {
        fun onClick(data: PlantInfo)
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): AreaDetailViewHolder {
        return AreaDetailViewHolder(
            ItemPlantBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: AreaDetailViewHolder, position: Int) {
        holder.bind(getItem(position), listener)
    }

    private class DiffCallback : DiffUtil.ItemCallback<PlantInfo>() {
        override fun areItemsTheSame(
            oldItem: PlantInfo,
            newItem: PlantInfo
        ): Boolean {
            return oldItem.name == newItem.name
        }

        override fun areContentsTheSame(
            oldItem: PlantInfo,
            newItem: PlantInfo
        ): Boolean {
            return oldItem == newItem
        }
    }

    inner class AreaDetailViewHolder(
        private val binding: ItemPlantBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(data: PlantInfo, listener: PlantClickListener) {
            bindImageWithPlaceholder(binding.image, data.imageUrl)
            binding.plantName.text = data.name
            binding.plantInfo.text = data.brief
            binding.plant.setOnClickListener {
                listener.onClick(data)
            }
        }
    }
}
