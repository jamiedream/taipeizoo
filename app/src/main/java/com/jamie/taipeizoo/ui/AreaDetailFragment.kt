package com.jamie.taipeizoo.ui

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.jamie.taipeizoo.data.PlantInfo
import com.jamie.taipeizoo.databinding.FragmentAreaDetailBinding
import com.jamie.taipeizoo.util.bindImageWithPlaceholder

class AreaDetailFragment : Fragment() {

    private lateinit var binding: FragmentAreaDetailBinding

    private val args: AreaDetailFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        binding = FragmentAreaDetailBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val areaDetailListAdapter = AreaDetailListAdapter(
            object : AreaDetailListAdapter.PlantClickListener {
                override fun onClick(data: PlantInfo) {
                    NavHostFragment.findNavController(this@AreaDetailFragment)
                        .navigate(
                            AreaDetailFragmentDirections.actionAreaDetailToPlant(data)
                        )
                }
            }
        )
        binding.listAreaDetail.apply {
            adapter = areaDetailListAdapter
            layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
            addItemDecoration(
                DividerItemDecoration(
                    requireContext(),
                    DividerItemDecoration.VERTICAL
                )
            )
        }

        binding.back.setOnClickListener { requireActivity().onBackPressed() }
        args.areaWithPlants.areaInfo.apply {
            binding.areaName.text = name
            binding.detail.let { detail ->
                bindImageWithPlaceholder(detail.image, imageUrl)
                detail.areaDetailCategory.text = category
                detail.areaInfo.text = info
                detail.areaMemo.text = memo
                detail.areaBrowser.setOnClickListener {
                    requireActivity().startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(url)))
                }
            }
        }

        args.areaWithPlants.plants.apply {
            if (this.isEmpty()) {
                binding.gap.isVisible = false
                binding.plantSectionTitle.isVisible = false
                binding.listAreaDetail.isVisible = false
            } else {
                areaDetailListAdapter.submitList(this)
            }
        }
    }
}
