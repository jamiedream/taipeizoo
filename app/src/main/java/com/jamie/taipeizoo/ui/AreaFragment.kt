package com.jamie.taipeizoo.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.NavHostFragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.jamie.taipeizoo.DataViewModel
import com.jamie.taipeizoo.R
import com.jamie.taipeizoo.data.AreaInfo
import com.jamie.taipeizoo.data.AreaWithPlants
import com.jamie.taipeizoo.databinding.FragmentAreaBinding
import com.jamie.taipeizoo.network.Status
import timber.log.Timber

class AreaFragment : Fragment() {

    private lateinit var binding: FragmentAreaBinding
    private val dataViewModel by viewModels<DataViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        binding = FragmentAreaBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val areaListAdapter = AreaListAdapter(
            object : AreaListAdapter.AreaClickListener {
                override fun onClick(data: AreaInfo) {
                    dataViewModel.areaWithPlants.value?.data?.filter { it.key == data }?.apply {
                        findNavController(this@AreaFragment)
                            .navigate(
                                AreaFragmentDirections.actionAreaToAreaDetail(
                                    AreaWithPlants(
                                        data,
                                        this[data] ?: emptyList()
                                    )
                                )
                            )
                    }
                }
            }
        )

        binding.listArea.apply {
            adapter = areaListAdapter
            layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
            addItemDecoration(
                DividerItemDecoration(
                    requireContext(),
                    DividerItemDecoration.VERTICAL
                )
            )
        }

        dataViewModel.areaList.observe(viewLifecycleOwner) {
            Timber.d("areaList ${it.status}")

            when (it.status) {
                Status.SUCCESS -> {
                    if (it.data == null) {
                        binding.progress.isVisible = false
                        binding.pageDataMessage.isVisible = true
                    } else {
                        dataViewModel.getPlantsData()
                        areaListAdapter.submitList(it.data)
                    }
                }
                Status.ERROR -> {
                    binding.progress.isVisible = false

                    binding.pageDataMessage.text = getString(R.string.error)
                    binding.retryButton.setOnClickListener {
                        binding.layoutPageMessage.isVisible = false
                        dataViewModel.getAreaData()
                    }
                    binding.layoutPageMessage.isVisible = true
                }
                Status.LOADING -> {
                    binding.progress.isVisible = true
                }
            }
        }

        dataViewModel.plantList.observe(viewLifecycleOwner) {
            when (it.status) {
                Status.SUCCESS -> {
                    dataViewModel.getPlantsListWithArea()
                }
                Status.ERROR -> {
                    binding.progress.isVisible = false

                    binding.pageDataMessage.text = getString(R.string.error)
                    binding.retryButton.setOnClickListener {
                        binding.layoutPageMessage.isVisible = false
                        dataViewModel.getAreaData()
                    }
                    binding.layoutPageMessage.isVisible = true
                }
                else -> {
                }
            }
        }

        dataViewModel.areaWithPlants.observe(viewLifecycleOwner) {
            if (it.status == Status.SUCCESS) {
                binding.progress.isVisible = false
            }
        }

        dataViewModel.getAreaData()
    }
}
