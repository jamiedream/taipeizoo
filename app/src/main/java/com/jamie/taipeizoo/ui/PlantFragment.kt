package com.jamie.taipeizoo.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.jamie.taipeizoo.R
import com.jamie.taipeizoo.databinding.FragmentPlantBinding
import com.jamie.taipeizoo.util.bindImageWithPlaceholder

class PlantFragment : Fragment() {

    private lateinit var binding: FragmentPlantBinding
    private val args: PlantFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        binding = FragmentPlantBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.back.setOnClickListener { requireActivity().onBackPressed() }
        args.plantInfo.apply {
            binding.plantName.text = name
            bindImageWithPlaceholder(binding.image, imageUrl)
            binding.nameCh.text = name
            binding.nameEn.text = nameEn
            binding.alsoKnown.text = alsoKnown
            binding.brief.text = brief
            binding.feature.text = feature
            binding.function.text = functionAndApplication
            binding.updateDate.text = getString(R.string.plant_update_date, updateDate)
        }
    }
}
