package com.jamie.taipeizoo.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.jamie.taipeizoo.data.AreaInfo
import com.jamie.taipeizoo.databinding.ItemAreaBinding
import com.jamie.taipeizoo.util.bindImageWithPlaceholder

class AreaListAdapter(private val listener: AreaClickListener) :
    ListAdapter<AreaInfo, AreaListAdapter.AreaViewHolder>(DiffCallback()) {

    interface AreaClickListener {
        fun onClick(data: AreaInfo)
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): AreaViewHolder {
        return AreaViewHolder(
            ItemAreaBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: AreaViewHolder, position: Int) {
        holder.bind(getItem(position), listener)
    }

    private class DiffCallback : DiffUtil.ItemCallback<AreaInfo>() {
        override fun areItemsTheSame(
            oldItem: AreaInfo,
            newItem: AreaInfo
        ): Boolean {
            return oldItem.name == newItem.name
        }

        override fun areContentsTheSame(
            oldItem: AreaInfo,
            newItem: AreaInfo
        ): Boolean {
            return oldItem == newItem
        }
    }

    inner class AreaViewHolder(
        private val binding: ItemAreaBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(data: AreaInfo, listener: AreaClickListener) {
            bindImageWithPlaceholder(binding.image, data.imageUrl)
            binding.areaName.text = data.name
            binding.areaInfo.text = data.info
            if (data.memo.isNullOrBlank().not()) {
                binding.areaMemo.text = data.memo
            }
            binding.area.setOnClickListener {
                listener.onClick(data)
            }
        }
    }
}
