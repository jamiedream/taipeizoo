package com.jamie.taipeizoo.data

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.parcelize.Parcelize

@Parcelize
data class AreaData(
    @Json(name = "limit") val limit: Int,
    @Json(name = "offset") val offset: Int,
    @Json(name = "count") val count: Int,
    @Json(name = "sort") val sort: String,
    @Json(name = "results") val results: List<AreaInfo>
) : Parcelable
