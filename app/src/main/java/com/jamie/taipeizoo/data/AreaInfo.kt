package com.jamie.taipeizoo.data

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.parcelize.Parcelize

@Parcelize
data class AreaInfo(
    @Json(name = "E_no") val num: String,
    @Json(name = "E_Category") val category: String,
    @Json(name = "E_Name") val name: String,
    @Json(name = "E_Pic_URL") val imageUrl: String,
    @Json(name = "E_Info") val info: String,
    @Json(name = "E_Memo") val memo: String,
    @Json(name = "E_Geo") val coordinator: String,
    @Json(name = "E_URL") val url: String
) : Parcelable
