package com.jamie.taipeizoo.data

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.parcelize.Parcelize

@Parcelize
data class Area(
    @Json(name = "result") val areaData: AreaData
) : Parcelable
