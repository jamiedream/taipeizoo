package com.jamie.taipeizoo.data

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class AreaWithPlants(
    val areaInfo: AreaInfo,
    val plants: List<PlantInfo>
) : Parcelable
