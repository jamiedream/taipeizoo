package com.jamie.taipeizoo.data

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.parcelize.Parcelize

@Parcelize
data class Plant(
    @Json(name = "result") val plantData: PlantData
) : Parcelable
