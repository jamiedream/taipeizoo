package com.jamie.taipeizoo.data

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.parcelize.Parcelize

@Parcelize
data class PlantInfo(
    @Json(name = "F_Name_Ch") val name: String? = null,
    @Json(name = "F_AlsoKnown") val alsoKnown: String,
    @Json(name = "F_Geo") val geo: String,
    @Json(name = "F_Location") val location: String,
    @Json(name = "F_Name_En") val nameEn: String,
    @Json(name = "F_Family") val family: String,
    @Json(name = "F_Genus") val genus: String,
    @Json(name = "F_Brief") val brief: String,
    @Json(name = "F_Feature") val feature: String,
    @Json(name = "F_Function＆Application") val functionAndApplication: String,
    @Json(name = "F_Pic01_ALT") val image: String,
    @Json(name = "F_Pic01_URL") val imageUrl: String,
    @Json(name = "F_Update") val updateDate: String,
    @Json(name = "F_CID") val cid: String,
) : Parcelable
