package com.jamie.taipeizoo

import android.app.Application
import timber.log.Timber

class ZooApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        // init Timber
        Timber.plant(Timber.DebugTree())
    }
}
