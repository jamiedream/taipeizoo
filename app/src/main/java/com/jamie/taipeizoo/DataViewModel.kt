package com.jamie.taipeizoo

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.jamie.taipeizoo.data.AreaInfo
import com.jamie.taipeizoo.data.PlantInfo
import com.jamie.taipeizoo.network.Resource
import com.jamie.taipeizoo.network.RetrofitUtils
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlin.collections.set

class DataViewModel : ViewModel() {

    private val _areaList = MutableLiveData<Resource<List<AreaInfo>>>()
    val areaList: LiveData<Resource<List<AreaInfo>>>
        get() = _areaList

    private val _plantsList = MutableLiveData<Resource<List<PlantInfo>>>()
    val plantList: LiveData<Resource<List<PlantInfo>>>
        get() = _plantsList

    var isPlantEmpty = true

    private val _areaWithPlants =
        MutableLiveData<Resource<Map<AreaInfo, List<PlantInfo>>>>(Resource.loading())
    val areaWithPlants: LiveData<Resource<Map<AreaInfo, List<PlantInfo>>>>
        get() = _areaWithPlants

    fun getAreaData() {
        _areaList.postValue(Resource.loading())
        viewModelScope.launch(Dispatchers.IO) {
            try {
                RetrofitUtils.getZooData().getAreas().let {
                    _areaList.postValue(Resource.success(it.areaData.results))
                }
            } catch (exception: Exception) {
                _areaList.postValue(Resource.error("Get area data failed."))
            }
        }
    }

    fun getPlantsData() {
        _plantsList.postValue(Resource.loading())
        viewModelScope.launch(Dispatchers.IO) {
            try {
                RetrofitUtils.getZooData().getPlants().let {
                    isPlantEmpty = it.plantData.results.isNullOrEmpty()
                    _plantsList.postValue(Resource.success(it.plantData.results))
                }
            } catch (exception: Exception) {
                _plantsList.postValue(Resource.error("Get plant data failed."))
            }
        }
    }

    fun getPlantsListWithArea() {
        val map = mutableMapOf<AreaInfo, MutableList<PlantInfo>>()
        _areaList.value?.data?.forEach {
            map[it] = mutableListOf()
        }
        if (isPlantEmpty.not()) {
            _plantsList.value?.data?.forEach { plantInfo ->
                val locationList = plantInfo.location.split("；")
                map.forEach {
                    val areaName = it.key.name
                    locationList.contains(areaName)
                    it.value.add(plantInfo)
                }
            }
        }

        _areaWithPlants.postValue(Resource.success(map))
    }
}
