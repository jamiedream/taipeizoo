package com.jamie.taipeizoo.util

import android.net.Uri
import android.widget.ImageView
import com.jamie.taipeizoo.GlideApp
import com.jamie.taipeizoo.R

fun bindImageWithPlaceholder(view: ImageView, imageUrl: String?) {
    view.loadImageUrlWithPlaceHolder(imageUrl)
}

private fun ImageView.loadImageUrlWithPlaceHolder(imageUrl: String?) {
    if (imageUrl.isNullOrEmpty().not()) {
        GlideApp.with(context)
            .load(Uri.parse(imageUrl))
            .into(this)
    }
}
